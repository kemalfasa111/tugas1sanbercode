import React from 'react';
import Card from '../card/Card';
import './cardContainer.css';

function CardContainer({ data }) {
	return (
		<div style={{ maxWidth: '80%', margin: 'auto' }}>
			<h1>Catalog Product</h1>
			<div className="card-container">
				{data.length === 0 ? <h3>Tidak ada product</h3> : data.map(item => <Card data={item} />)}
			</div>
		</div>
	);
}

export default CardContainer;
