import axios from 'axios';
import React, { useState } from 'react';
import './form.css';

function Form({ fetchProducts }) {
	const initialState = {
		nama: '',
		harga: 0,
		harga_diskon: 0,
		is_diskon: false,
		image_url: '',
		stock: 0,
	};
	const [input, setInput] = useState(initialState);

	const handleChangeCheckbox = e => {
		setInput({ ...input, is_diskon: e.target.checked });
	};

	const handleChangeInput = e => {
		setInput({ ...input, [e.target.name]: e.target.value });
	};

	const submitProduct = async () => {
		try {
			const response = await axios.post('http://arhandev.xyz/public/api/products', input);
			fetchProducts();
			setInput(initialState);
		} catch (e) {
			console.log(e);
			alert(e.response.data.info);
		}
	};
	return (
		<div className="form">
			<h1 style={{ textAlign: 'center', margin: '0px', marginBottom: '20px' }}>Membuat Product</h1>
			<form style={{ display: 'flex', flexDirection: 'column', gap: '20px' }}>
				<div className="form-input-group">
					<label>Nama: </label>
					<input
						type="text"
						name="nama"
						placeholder="Masukan nama barang"
						value={input.nama}
						onChange={handleChangeInput}
					/>
				</div>
				<div className="form-input-group">
					<label>Harga: </label>
					<input type="number" name="harga" value={input.harga} onChange={handleChangeInput} />
				</div>
				<div className="form-input-group">
					<label>Aktikan Diskon </label>
					<div className="checkbox">
						<input
							type="checkbox"
							onChange={handleChangeCheckbox}
							name="is_diskon"
							id=""
							defaultValue={input.is_diskon}
							checked={input.is_diskon}
						/>
					</div>
				</div>
				{input.is_diskon && (
					<div className="form-input-group">
						<label>Harga Diskon: </label>
						<input type="number" name="harga_diskon" value={input.harga_diskon} onChange={handleChangeInput} />
					</div>
				)}
				<div className="form-input-group">
					<label>Stock: </label>
					<input type="number" name="stock" value={input.stock} onChange={handleChangeInput} />
				</div>
				<div className="form-input-group">
					<label>Image: </label>
					<input
						type="text"
						name="image_url"
						placeholder="Masukan link gambar barang"
						value={input.image_url}
						onChange={handleChangeInput}
					/>
				</div>
				<div style={{ display: 'flex', justifyContent: 'center' }}>
					<button type="button" onClick={submitProduct}>
						Create
					</button>
				</div>
			</form>
		</div>
	);
}

export default Form;
