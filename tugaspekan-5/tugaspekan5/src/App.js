import axios from 'axios';
import { useEffect, useState } from 'react';
import './App.css';
import CardContainer from './components/cardContainer/CardContainer';
import Form from './components/form/Form';
import Loading from './components/loading/Loading';

function App() {
	const [data, setData] = useState([]);
	const [loading, setLoading] = useState(false);
	const fetchProducts = async () => {
		try {
			setLoading(true);
			const response = await axios.get('http://arhandev.xyz/public/api/products');
			setData(response.data.data);
			setLoading(false);
		} catch (e) {
			console.log(e);
			alert('terjadi suatu error');
			setLoading(false);
		}
	};
	useEffect(() => {
		fetchProducts();
	}, []);

	return (
		<div className="App">
			{loading && <Loading />}
			<Form fetchProducts={fetchProducts} />
			<CardContainer data={data} />
		</div>
	);
}

export default App;
