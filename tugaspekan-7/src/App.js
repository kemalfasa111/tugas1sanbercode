import { useContext } from 'react';
import { createBrowserRouter, RouterProvider } from 'react-router-dom';
import './App.css';
import CardContainer from './components/cardContainer/CardContainer';
import EditForm from './components/editForm/EditForm';
import Form from './components/form/Form';
import Loading from './components/loading/Loading';
import ProductTable from './components/productTable/ProductTable';
import { GlobalContext } from './context/GlobalContext';

const router = createBrowserRouter([
	{
		path: '/',
		element: <CardContainer />,
	},
	{
		path: '/table',
		element: <ProductTable />,
	},
	{
		path: '/create',
		element: <Form />,
	},
	{
		path: '/edit/:id',
		element: <EditForm />,
	},
]);

function App() {
	const { loading } = useContext(GlobalContext);
	return (
		<div className="App">
			{loading && <Loading />}
			<RouterProvider router={router} />
		</div>
	);
}

export default App;
