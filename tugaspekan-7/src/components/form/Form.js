import axios from 'axios';
import { Formik } from 'formik';
import React, { useContext } from 'react';
import { useNavigate } from 'react-router-dom';
import * as Yup from 'yup';
import { GlobalContext } from '../../context/GlobalContext';
import Navbar from '../loading/Navbar';



const validationSchema = Yup.object().shape({
	nama: Yup.string().required('Nama wajib diisi'),
	harga: Yup.number()
		.min(1000, 'Minimal harga adalah 1000')
		.typeError('Harap masukkan angka yang valid')
		.required('Harga wajib diisi'),
	stock: Yup.number()
		.min(1, 'Minimal stock adalah 1')
		.typeError('Harap masukkan stock yang valid')
		.required('stock wajib diisi'),
	is_diskon: Yup.boolean(),
	harga_diskon: Yup.number().when('is_diskon', {
		is: 1,
		then: Yup.number()
			.typeError('Harap masukkan angka yang valid')
			.min(1000, 'Minimal harga adalah 1000')
			.required('Harap Masukan harga diskon'),
	}),
	image_url: Yup.string().url().required('Gambar wajib diisi'),
});

const initialState = {
	nama: '',
	harga: 0,
	harga_diskon: 0,
	is_diskon: false,
	image_url: '',
	stock: 0,
};

function Form() {
	const navigate = useNavigate();
	const { setLoading } = useContext(GlobalContext);

	const onSubmit = async values => {
		setLoading(true);
		try {
			const response = await axios.post('https://arhandev.xyz/public/api/products', values);
			navigate('/table');
		} catch (e) {
			console.log(e);
			alert(e.response.data.info);
		} finally {
			setLoading(false);
		}
	};
	return (
		<div>
			<Navbar />
			<div className='my-0 mx-96 border-double border-2 border-black rounded-lg'>
				<h1 className='text-center font-bold text-2xl mt-2 mb-5'>Membuat Product</h1>
				<Formik initialValues={initialState} onSubmit={onSubmit} validationSchema={validationSchema}>
					{({ values, handleSubmit, handleBlur, handleChange, setFieldValue, errors, touched, dirty, isValid }) => (
						<form onSubmit={handleSubmit} style={{ display: 'flex', flexDirection: 'column', gap: '20px' }}>
							<div className="pl-10">
								<label>Nama: </label>
								<input
									type="text"
									name="nama"
									placeholder="Masukan nama barang"
									value={values.nama}
									onChange={handleChange}
									onBlur={handleBlur}
								/>
								<div></div>
								{touched.nama && errors.nama && <div style={{ color: 'red' }}> {errors.nama}</div>}
							</div>
							<div className='pl-10'>
								<label>Harga: </label>
								<input type="number" name="harga" value={values.harga} onChange={handleChange} onBlur={handleBlur} />
								<div></div>
								{touched.harga && errors.harga && <div style={{ color: 'red' }}> {errors.harga}</div>}
							</div>
							<div className='pl-10'>
								<label>Aktikan Diskon </label>
								<div className='pl-2'>
									<input
										type="checkbox"
										onChange={e => {
											if (e.target.checked) {
												setFieldValue('is_diskon', 1);
											} else {
												setFieldValue('is_diskon', 0);
											}
										}}
										name="is_diskon"
										id=""
										checked={values.is_diskon}
										onBlur={handleBlur}
									/>
								</div>
								<div></div>
								{touched.is_diskon && errors.is_diskon && <div style={{ color: 'red' }}> {errors.is_diskon}</div>}
							</div>
							{values.is_diskon === 1 && (
								<div className='pl-10'>
									<label>Harga Diskon: </label>
									<input
										type="number"
										name="harga_diskon"
										value={values.harga_diskon}
										onChange={handleChange}
										onBlur={handleBlur}
									/>
									<div></div>
									{touched.harga_diskon && errors.harga_diskon && (
										<div style={{ color: 'red' }}> {errors.harga_diskon}</div>
									)}
								</div>
							)}
							<div className='pl-10'>
								<label>Stock: </label>
								<input type="number" name="stock" value={values.stock} onChange={handleChange} onBlur={handleBlur} />
								<div></div>
								{touched.stock && errors.stock && <div style={{ color: 'red' }}> {errors.stock}</div>}
							</div>
							<div className='pl-10'>
								<label>Image: </label>
								<input
									type="text"
									name="image_url"
									placeholder="Masukan link gambar barang"
									value={values.image_url}
									onChange={handleChange}
									onBlur={handleBlur}
								/>
								<div></div>
								{touched.image_url && errors.image_url && <div style={{ color: 'red' }}> {errors.image_url}</div>}
							</div>
							<div className='rounded-full mb-6 px-40'>
								<button disabled={!dirty && !isValid}>Create</button>
							</div>
						</form>
					)}
				</Formik>
			</div>
		</div>
	);
}

export default Form;
