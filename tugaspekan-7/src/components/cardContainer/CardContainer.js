import axios from 'axios';
import React, { useContext, useEffect } from 'react';
import { GlobalContext } from '../../context/GlobalContext';
import Card from '../card/Card';
import Navbar from '../loading/Navbar';
import './cardContainer.css';

function CardContainer() {
	const { products, setProducts, setLoading } = useContext(GlobalContext);

	const fetchProducts = async () => {
		try {
			setLoading(true);
			const response = await axios.get('https://arhandev.xyz/public/api/products');
			setProducts(response.data.data);
			setLoading(false);
		} catch (e) {
			console.log(e);
			alert('terjadi suatu error');
			//cara terbaik untuk setLoading false adalah memasukkannya ke dalam block finally
			// pelajarin lebih lanjut mengenai block finally
			setLoading(false);
		}
	};
	useEffect(() => {
		fetchProducts();
	}, []);

	return (
		<div>
			<Navbar />
			<div style={{ maxWidth: '80%', margin: 'auto' }}>
				<h1>Catalog Product</h1>
				<div className="card-container">
					{products.length === 0 ? (
						<h3>Tidak ada product</h3>
					) : (
						products.map((item, index) => <Card key={index} data={item} />)
					)}
				</div>
			</div>
		</div>
	);
}

export default CardContainer;
