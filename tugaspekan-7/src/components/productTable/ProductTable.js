import axios from 'axios';
import React, { useContext, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { GlobalContext } from '../../context/GlobalContext';
import Navbar from '../loading/Navbar';
import './productTable.css';

function ProductTable() {
	const { products, setEdit, setProducts, setLoading, loading } = useContext(GlobalContext);
	const onDelete = async productId => {
		try {
			const response = await axios.delete(`https://arhandev.xyz/public/api/products/${productId}`);
			fetchProducts();
		} catch (e) {
			console.log(e);
			alert(e.response.data.info);
		}
	};

	const fetchProducts = async () => {
		try {
			setLoading(true);
			const response = await axios.get('https://arhandev.xyz/public/api/products');
			setProducts(response.data.data);
			setLoading(false);
		} catch (e) {
			console.log(e);
			alert('terjadi suatu error');
			//cara terbaik untuk setLoading false adalah memasukkannya ke dalam block finally
			// pelajarin lebih lanjut mengenai block finally
			setLoading(false);
		}
	};
	useEffect(() => {
		fetchProducts();
	}, []);
	return (
		<div>
			<Navbar />
			<div
				style={{ maxWidth: '900px', margin: 'auto', marginTop: '30px', display: 'flex', justifyContent: 'flex-end' }}
			>
				<Link to="/create">
					<button className="btn btn-create">
						<span style={{ fontSize: '15px' }}>+</span> Buat Product
					</button>
				</Link>
			</div>
			<table class='border-collapse border border-slate-400'>
				<thead>
					<tr>
						<th>Nama</th>
						<th>Harga</th>
						<th>Stock</th>
						<th>Status Diskon</th>
						<th>Harga Diskon</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					{products.map((product, index) => (
						<tr key={index}>
							<td>{product.nama}</td>
							<td>Rp. {product.harga_display}</td>
							<td>{product.stock}</td>
							<td>{product.is_diskon === 0 ? 'Tidak Aktif' : 'Aktif'}</td>
							<td>{product.is_diskon === 0 ? '-' : `Rp. ${product.harga_diskon_display}`}</td>
							<td>
								<div className="action">
									<Link to={`/edit/${product.id}`}>
										<button className="btn btn-edit">Edit</button>
									</Link>
									<button
										onClick={() => {
											onDelete(product.id);
										}}
										className="btn btn-delete"
									>
										Delete
									</button>
								</div>
							</td>
						</tr>
					))}
				</tbody>
			</table>
		</div>
	);
}

export default ProductTable;
