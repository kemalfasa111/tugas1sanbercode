import React from 'react';
import { Link } from 'react-router-dom';

function Navbar() {
	return (
		<div className='my-16 mx-1 rounded-md bg-slate-600 text-white text-2xl font-bold pt-2 py-2 pl-8 '>
			<nav className="navbar">
				<Link to="/">
					<div>Home</div>
				</Link>
				<Link to="/table">
					<div>Table</div>
				</Link>
			</nav>
		</div>
	);
}

export default Navbar;
