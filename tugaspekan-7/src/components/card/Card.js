import React from 'react';
import './card.css';

function Card({ data }) {
	return (
		<div className="card">
			<img style={{ width: '100%' }} src={data.image_url} alt="" />
			<div className="card-body">
				<p className="nama">{data.nama}</p>
				{data.is_diskon === 1 ? (
					<p className="harga-diskon">Rp {data.harga_diskon_display}</p>
				) : (
					<p className="harga">Rp {data.harga_display}</p>
				)}
				<p className="stock">Stock {data.stock}</p>
			</div>
		</div>
	);
}

export default Card;
