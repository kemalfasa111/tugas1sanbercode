import axios from 'axios';
import { Formik } from 'formik';
import React, { useContext, useEffect, useState } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import * as Yup from 'yup';
import { GlobalContext } from '../../context/GlobalContext';
import Navbar from '../loading/Navbar';

const validationSchema = Yup.object().shape({
	nama: Yup.string().required('Nama wajib diisi'),
	harga: Yup.number()
		.typeError('Harap masukkan angka yang valid')
		.min(1000, 'Minimal harga adalah 1000')
		.required('Harga wajib diisi'),
	stock: Yup.number()
		.typeError('Harap masukkan angka yang valid')
		.min(1, 'Minimal stock adalah 1')
		.required('stock wajib diisi'),
	is_diskon: Yup.boolean(),
	harga_diskon: Yup.number().when('is_diskon', {
		is: 1,
		then: Yup.number()
			.typeError('Harap masukkan angka yang valid')
			.min(1000, 'Minimal harga adalah 1000')
			.required('Harap Masukan harga diskon'),
	}),
	image_url: Yup.string().url().required('Gambar wajib diisi'),
});

function EditForm() {
	const { id } = useParams();
	const navigate = useNavigate();
	const { setLoading } = useContext(GlobalContext);

	const [input, setInput] = useState({
		nama: '',
		harga: 0,
		harga_diskon: 0,
		is_diskon: false,
		image_url: '',
		stock: 0,
	});

	const handleChangeCheckbox = e => {
		if (e.target.checked) {
			setInput({ ...input, is_diskon: 1 });
		} else {
			setInput({ ...input, is_diskon: 0 });
		}
	};

	const handleChangeInput = e => {
		setInput({ ...input, [e.target.name]: e.target.value });
	};

	const onUpdate = async values => {
		setLoading(true);
		try {
			const response = await axios.put(`https://arhandev.xyz/public/api/products/${id}`, values);
			navigate('/table');
		} catch (e) {
			console.log(e);
			alert(e.response.data.info);
		} finally {
			setLoading(false);
		}
	};

	const fetchProduct = async () => {
		setLoading(true);
		try {
			const response = await axios.get(`https://arhandev.xyz/public/api/products/${id}`);
			setInput({
				nama: response.data.data.nama,
				harga: response.data.data.harga,
				harga_diskon: response.data.data.harga_diskon,
				is_diskon: response.data.data.is_diskon,
				image_url: response.data.data.image_url,
				stock: response.data.data.stock,
			});
		} catch (e) {
			console.log(e);
			alert(e.response.data.info);
		} finally {
			setLoading(false);
		}
	};

	useEffect(() => {
		fetchProduct();
	}, []);

	return (
		<div>
			<Navbar />

			<div className="form">
				<h1 style={{ textAlign: 'center', margin: '0px', marginBottom: '20px' }}>Mengupdate Product</h1>
				<Formik initialValues={input} onSubmit={onUpdate} validationSchema={validationSchema} enableReinitialize>
					{({ values, handleSubmit, handleBlur, handleChange, setFieldValue, errors, touched, dirty, isValid }) => (
						<form onSubmit={handleSubmit} style={{ display: 'flex', flexDirection: 'column', gap: '20px' }}>
							<div className="form-input-group">
								<label>Nama: </label>
								<input
									type="text"
									name="nama"
									placeholder="Masukan nama barang"
									value={values.nama}
									onChange={handleChange}
									onBlur={handleBlur}
								/>
								<div></div>
								{touched.nama && errors.nama && <div style={{ color: 'red' }}> {errors.nama}</div>}
							</div>
							<div className="form-input-group">
								<label>Harga: </label>
								<input type="number" name="harga" value={values.harga} onChange={handleChange} onBlur={handleBlur} />
								<div></div>
								{touched.harga && errors.harga && <div style={{ color: 'red' }}> {errors.harga}</div>}
							</div>
							<div className="form-input-group">
								<label>Aktikan Diskon </label>
								<div className="checkbox">
									<input
										type="checkbox"
										onChange={e => {
											if (e.target.checked) {
												setFieldValue('is_diskon', 1);
											} else {
												setFieldValue('is_diskon', 0);
											}
										}}
										name="is_diskon"
										id=""
										checked={values.is_diskon}
										onBlur={handleBlur}
									/>
								</div>
								<div></div>
								{touched.is_diskon && errors.is_diskon && <div style={{ color: 'red' }}> {errors.is_diskon}</div>}
							</div>
							{values.is_diskon === 1 && (
								<div className="form-input-group">
									<label>Harga Diskon: </label>
									<input
										type="number"
										name="harga_diskon"
										value={values.harga_diskon}
										onChange={handleChange}
										onBlur={handleBlur}
									/>
									<div></div>
									{touched.harga_diskon && errors.harga_diskon && (
										<div style={{ color: 'red' }}> {errors.harga_diskon}</div>
									)}
								</div>
							)}
							<div className="form-input-group">
								<label>Stock: </label>
								<input type="number" name="stock" value={values.stock} onChange={handleChange} onBlur={handleBlur} />
								<div></div>
								{touched.stock && errors.stock && <div style={{ color: 'red' }}> {errors.stock}</div>}
							</div>
							<div className="form-input-group">
								<label>Image: </label>
								<input
									type="text"
									name="image_url"
									placeholder="Masukan link gambar barang"
									value={values.image_url}
									onChange={handleChange}
									onBlur={handleBlur}
								/>
								<div></div>
								{touched.image_url && errors.image_url && <div style={{ color: 'red' }}> {errors.image_url}</div>}
							</div>
							<div style={{ display: 'flex', justifyContent: 'center' }}>
								<button disabled={!isValid}>Save</button>
							</div>
						</form>
					)}
				</Formik>
			</div>
		</div>
	);
}

export default EditForm;
