import axios from "axios";
import React, { useContext, useEffect } from "react";
import { Link } from "react-router-dom";
import { GlobalContext } from "../../context/GlobalContext";
import Navbar from "../loading/Navbar";

function ProductTable() {
  const { products, setEdit, setProducts, setLoading, loading } =
    useContext(GlobalContext);
  const onDelete = async (productId) => {
    try {
      const response = await axios.delete(
        `https://arhandev.xyz/public/api/final/products/${productId}`,
        {
          headers: { Authorization: `Bearer ${localStorage.getItem("token")}` },
        }
      );
      fetchProducts();
    } catch (e) {
      console.log(e);
      alert(e.response.data.info);
    }
  };

  const fetchProducts = async () => {
    try {
      setLoading(true);
      const response = await axios.get(
        "https://arhandev.xyz/public/api/final/products",
        {
          headers: { Authorization: `Bearer ${localStorage.getItem("token")}` },
        }
      );
      setProducts(response.data.data);
      setLoading(false);
    } catch (e) {
      console.log(e);
      alert("terjadi suatu error");
      setLoading(false);
    }
  };
  useEffect(() => {
    fetchProducts();
  }, []);
  return (
    <div>
      <Navbar />
      <div className="max-w-5xl mx-auto mt-8 flex justify-end">
        <Link to="/create">
          <button className="px-5 py-3 text-white font-bold rounded-lg bg-blue-600 flex items-center gap-3">
            <span className="text-2xl">+</span> <span>Buat Product</span>
          </button>
        </Link>
      </div>
      <table className="max-w-5xl mx-auto w-full border-collapse mt-8 table-auto">
        <thead>
          <tr className="text-white font-bold text-left text-xl bg-blue-700">
            <th className="p-2 border-2 border-blue-700">Nama</th>
            <th className="p-2 border-2 border-blue-700">Harga</th>
            <th className="p-2 border-2 border-blue-700">Stock</th>
            <th className="p-2 border-2 border-blue-700">Status Diskon</th>
            <th className="p-2 border-2 border-blue-700">Harga Diskon</th>
            <th className="p-2 border-2 border-blue-700">Dibuat Oleh</th>
            <th className="p-2 border-2 border-blue-700">Action</th>
          </tr>
        </thead>
        <tbody>
          {products.map((product, index) => (
            <tr key={index}>
              <td className="p-2 border-2 border-blue-700">{product.nama}</td>
              <td className="p-2 border-2 border-blue-700">
                Rp. {product.harga_display}
              </td>
              <td className="p-2 border-2 border-blue-700">{product.stock}</td>
              <td className="p-2 border-2 border-blue-700">
                {product.is_diskon === 0 ? "Tidak Aktif" : "Aktif"}
              </td>
              <td className="p-2 border-2 border-blue-700">
                {product.is_diskon === 0
                  ? "-"
                  : `Rp. ${product.harga_diskon_display}`}
              </td>
              <td className="p-2 border-2 border-blue-700">
                {product.user?.username}
              </td>
              <td className="p-2 border-2 border-blue-700">
                <div className="flex gap-2">
                  <Link to={`/edit/${product.id}`}>
                    <button className="px-5 py-3 text-white font-bold rounded-lg bg-yellow-600">
                      Edit
                    </button>
                  </Link>
                  <button
                    onClick={() => {
                      onDelete(product.id);
                    }}
                    className="px-5 py-3 text-white font-bold rounded-lg bg-red-600"
                  >
                    Delete
                  </button>
                </div>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
}

export default ProductTable;
