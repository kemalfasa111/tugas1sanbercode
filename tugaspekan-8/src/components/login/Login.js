import axios from "axios";
import { Formik } from "formik";
import React from "react";
import { useNavigate } from "react-router-dom";
import * as Yup from "yup";
import Navbar from "../Navbar";

const validationSchema = Yup.object({
  email: Yup.string().required("Email wajib diisi").email("Email tidak valid"),
  password: Yup.string().required("Password wajib diisi"),
});

function Login() {
  const initialState = {
    email: "",
    password: "",
  };
  const navigate = useNavigate();
  const onSubmit = async (values) => {
    try {
      const response = await axios.post(
        "https://arhandev.xyz/public/api/final/login",
        {
          email: values.email,
          password: values.password,
        }
      );
      localStorage.setItem("token", response.data.data.token);
      localStorage.setItem("username", response.data.data.user.username);
      navigate("/");
    } catch (error) {
      alert(error.response.data.info);
    }
  };
  return (
    <div>
      <Navbar />
      <div className="max-w-xl mx-auto border border-green-600 rounded-xl p-8 mt-16">
        <h1 className="text-center mb-6 text-2xl font-bold">Login Form</h1>
        <Formik
          onSubmit={onSubmit}
          initialValues={initialState}
          validationSchema={validationSchema}
        >
          {({
            handleSubmit,
            handleBlur,
            handleChange,
            values,
            errors,
            touched,
          }) => (
            <form
              onSubmit={handleSubmit}
              className="text-lg flex flex-col gap-2 items-center"
            >
              <div className="w-full grid grid-cols-3">
                <label>Email:</label>
                <input
                  className="col-span-2 border rounded-lg border-gray-400 py-0.5 px-2"
                  onChange={handleChange}
                  value={values.email}
                  onBlur={handleBlur}
                  type="text"
                  name="email"
                />
                <div></div>
                <div className="col-span-2 text-red-400 mb-3 text-base">
                  {touched.email && errors.email}
                </div>
              </div>
              <div className="w-full grid grid-cols-3">
                <label>Password:</label>
                <input
                  className="col-span-2 border rounded-lg border-gray-400 py-0.5 px-2"
                  type="password"
                  onChange={handleChange}
                  value={values.password}
                  onBlur={handleBlur}
                  name="password"
                />
                <div></div>
                <div className="col-span-2 text-red-400 mb-3 text-base">
                  {touched.password && errors.password}
                </div>
              </div>
              <button
                className="py-2 px-6 bg-green-600 rounded-lg text-white font-bold"
                type="submit"
              >
                Login
              </button>
            </form>
          )}
        </Formik>
      </div>
    </div>
  );
}

export default Login;
