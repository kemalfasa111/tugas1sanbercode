import React from "react";

function Card({ data }) {
  return (
    <div class="max-w-sm overflow-hidden rounded-xl bg-white shadow-md duration-200 hover:scale-105 hover:shadow-xl">
      <img
        src={data.image_url}
        alt="plant"
        class="h-64 object-contain w-full"
      />
      <div class="p-5">
        <div className="p-2">
          <p className="text-2xl font-bold">{data.nama}</p>
          {data.is_diskon === 1 ? (
            <div>
              <p className="text-blue-800 text-sm -mb-1 line-through">
                Rp {data.harga_display}
              </p>
              <p className="text-xl text-red-500 font-bold">
                Rp {data.harga_diskon_display}
              </p>
            </div>
          ) : (
            <p className="text-xl text-blue-800 font-bold">
              Rp {data.harga_display}
            </p>
          )}
          <p className="text-lg text-blue-500">Stock {data.stock}</p>
        </div>
      </div>
    </div>
  );
}

export default Card;
