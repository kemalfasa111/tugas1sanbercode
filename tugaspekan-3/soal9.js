const userData = {
    nama: 'Farhan',
    user_type: 'free'
};

const data = userData  => {
    return new Promise((resolve, reject) => {
        if (userData == 'free') {
            resolve('Maaf Kamu tidak punya hak untuk mengakses ini');
        } else if (userData == 'pro') {
            resolve('Silakan masuk dan buat produk yang kamu inginkan');
        } else 
            reject('user type tidak diketahui');
    });
};

data (userData.user_type)
    .then(value  => {
        console.log ('Berhasil');
        console.log (value);
    })
    .catch(error => {
        console.log ('Error!');
        console.log (error);
    });