let arr = [ 5, 4, 9, 11, 8, 7, 13];
let multipleArr = arr.map ((value, index) => {
    if (index %2 === 0){ 
        return value * 2;
    } else {
        return value * 3;
    }
});

console.log (multipleArr);